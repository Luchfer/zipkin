package br.com.mastertech.searchcep.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O cep não foi encontrado")
public class CepNotFoundException extends RuntimeException{

}
