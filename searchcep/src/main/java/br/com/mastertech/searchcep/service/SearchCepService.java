package br.com.mastertech.searchcep.service;

import br.com.mastertech.searchcep.client.Cep;
import br.com.mastertech.searchcep.client.CepClient;
import br.com.mastertech.searchcep.exception.CepNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchCepService {
    @Autowired
    private CepClient cepClient;

    public Cep getCep(String cep){
        Cep dados = new Cep();
        try{
            dados = cepClient.getCep(cep);
        } catch(Exception e){
            throw new CepNotFoundException();
        }
        return dados;
    }

}
