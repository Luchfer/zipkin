package br.com.mastertech.searchcep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class SearchcepApplication {

	public static void main(String[] args) {
		SpringApplication.run(SearchcepApplication.class, args);
	}

}
