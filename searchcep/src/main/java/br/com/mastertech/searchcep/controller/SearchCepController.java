package br.com.mastertech.searchcep.controller;

import br.com.mastertech.searchcep.client.Cep;
import br.com.mastertech.searchcep.service.SearchCepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cep")
public class SearchCepController {
   @Autowired
    private SearchCepService searchService;

   @GetMapping("{cep}")
    public Cep getCep(@PathVariable String cep){
       Cep dados = searchService.getCep(cep);
       return dados;
   }

}
