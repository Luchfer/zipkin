package br.com.mastertech.user.service;

import br.com.mastertech.user.model.User;
import br.com.mastertech.user.repository.UserRepository;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;

public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User createUser(User dataUser){


        return userRepository.save(dataUser);
    }

}
