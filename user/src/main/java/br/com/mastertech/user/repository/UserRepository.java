package br.com.mastertech.user.repository;

import br.com.mastertech.user.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
