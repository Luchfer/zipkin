package br.com.mastertech.user.client;

import br.com.mastertech.user.client.model.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.ws.rs.Path;

@FeignClient(name="searchCep")
public interface CepClient {

    @GetMapping("cep/{cep}")
    Cep getCep(@PathVariable String cep);
}
